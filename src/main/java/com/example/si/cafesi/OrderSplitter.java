package com.example.si.cafesi;

import com.example.si.cafesi.model.Order;
import com.example.si.cafesi.model.OrderItem;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Splitter;

import java.util.List;
import java.util.stream.Collectors;

@MessageEndpoint
public class OrderSplitter {
    @Splitter(inputChannel = Channels.ORDERS, outputChannel = Channels.ORDERITEMS)
    public List<OrderItem> split(Order order) {
        return order.getOrderItems().stream().map(orderItem -> {
            orderItem.setCorrelationId(order.getId());
            return orderItem;
        }).collect(Collectors.toList());
    }
}
