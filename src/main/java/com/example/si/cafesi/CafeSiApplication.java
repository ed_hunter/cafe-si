package com.example.si.cafesi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CafeSiApplication {
    public static void main(String[] args) {
        SpringApplication.run(CafeSiApplication.class, args);
    }
}
