package com.example.si.cafesi;

public final class Channels {
    public final static String ORDERS = "orders";
    public final static String ORDERITEMS = "orderItems";
    public final static String COLDDRINKORDERITEMS = "coldDrinkOrderItems";
    public final static String HOTDRINKORDERITEMS = "hotDrinkOrderItems";
    public final static String COLDDRINKS = "coldDrinks";
    public final static String HOTDRINKS = "hotDrinks";
    public final static String READYDRINKS = "readyDrinks";
    public final static String DELIVERIES = "deliveries";
}
