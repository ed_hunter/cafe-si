package com.example.si.cafesi;

import com.example.si.cafesi.model.OrderItem;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Router;

@MessageEndpoint
public class OrderItemRouter {

    @Router(inputChannel = Channels.ORDERITEMS)
    public String routeDrinkOrders(OrderItem orderItem) {
        return orderItem.isIced() ? Channels.COLDDRINKORDERITEMS : Channels.HOTDRINKORDERITEMS;
    }
}
