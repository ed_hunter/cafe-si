package com.example.si.cafesi;

import com.example.si.cafesi.model.DrinkType;
import com.example.si.cafesi.model.Order;
import com.example.si.cafesi.model.OrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class CafeRunner implements ApplicationRunner {
    @Autowired
    private Cafe cafe;

    @Override
    public void run(ApplicationArguments args) {
        for (int i = 0; i <= 100; i++) {
            Order order = new Order();
            order.setId(i);
            OrderItem mocha = OrderItem.builder().drinkType(DrinkType.MOCHA).iced(true).correlationId(i).build();
            OrderItem espresso = OrderItem.builder().drinkType(DrinkType.ESPRESSO).iced(false).correlationId(i).build();
            order.setOrderItems(List.of(mocha, espresso));
            log.info("<<<Input>>>: " + order);
            cafe.placeOrder(order);
        }
    }
}
