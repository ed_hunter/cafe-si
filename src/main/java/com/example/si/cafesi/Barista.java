package com.example.si.cafesi;

import com.example.si.cafesi.model.Drink;
import com.example.si.cafesi.model.OrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;

@MessageEndpoint
@Slf4j
public class Barista {

    @ServiceActivator(inputChannel = Channels.HOTDRINKS, outputChannel = Channels.READYDRINKS)
    public Drink prepareHotDrink(OrderItem orderItem)  {
        log.info(">>>Preparing hot: " + orderItem);
        delay(3000);
        return prepareDrink(orderItem);
    }

    @ServiceActivator(inputChannel = Channels.COLDDRINKS, outputChannel = Channels.READYDRINKS)
    public Drink prepareColdDrink(OrderItem orderItem) {
        log.info(">>>Preparing cold: " + orderItem);
        delay(2000);
        return prepareDrink(orderItem);
    }

    private void delay(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // ignore
        }
    }

    private Drink prepareDrink(OrderItem orderItem) {
        return Drink.builder()
                .iced(orderItem.isIced())
                .correlationId(orderItem.getCorrelationId())
                .drinkType(orderItem.getDrinkType())
                .build();
    }
}
