package com.example.si.cafesi;


import com.example.si.cafesi.model.Delivery;
import com.example.si.cafesi.model.Drink;
import org.springframework.integration.annotation.Aggregator;
import org.springframework.integration.annotation.CorrelationStrategy;
import org.springframework.integration.annotation.MessageEndpoint;

import java.util.List;

@MessageEndpoint
public class Waiter {
    @Aggregator(inputChannel = Channels.READYDRINKS, outputChannel = Channels.DELIVERIES)
    public Delivery prepareDelivery(List<Drink> drinks) {
        Delivery delivery = new Delivery();
        delivery.setDrinks(drinks);
        delivery.setOrderId(drinks.get(0).getCorrelationId());
        return delivery;
    }

    @CorrelationStrategy
    public int correlateByOrderNumber(Drink drink) {
        return drink.getCorrelationId();
    }
}
