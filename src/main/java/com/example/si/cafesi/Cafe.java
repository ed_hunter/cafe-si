package com.example.si.cafesi;

import com.example.si.cafesi.model.Order;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface Cafe {
    @Gateway(requestChannel = Channels.ORDERS)
    void placeOrder(Order order);
}
