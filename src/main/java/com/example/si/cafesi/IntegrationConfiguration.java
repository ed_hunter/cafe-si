package com.example.si.cafesi;

import com.example.si.cafesi.model.Delivery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.dsl.Pollers;
import org.springframework.messaging.MessageChannel;

import java.time.Duration;

@EnableIntegration
@Configuration
@Slf4j
public class IntegrationConfiguration {
    @Bean(name = Channels.ORDERS)
    public MessageChannel orders() {
        return new DirectChannel();
    }

    @Bean(name = Channels.ORDERITEMS)
    public MessageChannel orderItems() {
        return new DirectChannel();
    }

    @Bean(name = Channels.COLDDRINKORDERITEMS)
    public MessageChannel coldDrinkOrderItems() {
        return MessageChannels.queue(10).get();
    }

    @Bean(name = Channels.HOTDRINKORDERITEMS)
    public MessageChannel hotDrinkOrderItems() {
        return MessageChannels.queue(10).get();
    }

    @Bean(name = Channels.HOTDRINKS)
    public MessageChannel hotDrinks() {
        return new DirectChannel();
    }

    @Bean(name = Channels.COLDDRINKS)
    public MessageChannel coldDrinks() {
        return new DirectChannel();
    }

    @Bean(name = Channels.READYDRINKS)
    public MessageChannel readyDrinks() {
        return new DirectChannel();
    }

    @Bean(name = Channels.DELIVERIES)
    public MessageChannel deliveries() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow bridgeColdDrinks() {
        return IntegrationFlows
                .from(coldDrinkOrderItems())
                .bridge(c -> c.poller(Pollers.fixedDelay(Duration.ofSeconds(1))))
                .channel(coldDrinks())
                .get();
    }

    @Bean
    public IntegrationFlow bridgeHotDrinks() {
        return IntegrationFlows
                .from(hotDrinkOrderItems())
                .bridge(c -> c.poller(Pollers.fixedDelay(Duration.ofSeconds(1))))
                .channel(hotDrinks())
                .get();
    }

    @Bean
    public IntegrationFlow deliver() {
        return IntegrationFlows
                .from(deliveries())
                .handle(message -> logDelivery((Delivery) message.getPayload()))
                .get();
    }

    private void logDelivery(Delivery message) {
        log.info("======================" + message.getOrderId() + "======================");
        log.info(message.toString());
        log.info("======================000======================");
    }
}
