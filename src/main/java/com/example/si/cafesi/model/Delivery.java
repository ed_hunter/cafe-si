package com.example.si.cafesi.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Delivery {
    private int orderId;
    private List<Drink> drinks = new ArrayList<>();
}
