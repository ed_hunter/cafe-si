package com.example.si.cafesi.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Drink {
    private int correlationId;
    private boolean iced;
    private DrinkType drinkType;
}
