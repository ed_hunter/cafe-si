package com.example.si.cafesi.model;

public enum DrinkType {
    MOCHA,
    ESPRESSO,
    LATE
}
