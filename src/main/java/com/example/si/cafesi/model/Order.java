package com.example.si.cafesi.model;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class Order {
    private int id;
    private List<OrderItem> orderItems = new ArrayList<>();
}

