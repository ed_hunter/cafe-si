Café Sample Application
=======================

## Overview

The *Café Sample* is based on Spring Integration https://github.com/spring-projects/spring-integration-samples/blob/master/applications/cafe/

                                                                                        (Bridge)                     Barista                 
                                                                    hotDrinkOrderItems             hotDrinks    __________________
                                                                          |==========|--> |__| -->|========| ->|                  |
                       orders                 orderItems                 /                                     |prepareHotDrink() | readyDrinks         deliveries
	CafeRunner->Cafe->|======|->OrderSplitter->|======|->OrderItemRouter                                       |                  | ->|======|-> Waiter->|======|-> (print)
                                                                         \                         coldDrinks  |prepareColdDrink()|           (Aggregator)
                                                                          |==========|--> |__| -->|========| ->|                  |
                                                                  coldDrinkOrderItems                          |__________________|
	
	                                                Legend: |====| - channels
	                                                          |__| - queue
	                                                         
	                                                    